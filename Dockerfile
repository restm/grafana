FROM grafana/grafana

ENV PLUGINSDIR /grafana/plugins
ENV GF_PATHS_PLUGINS /grafana/plugins

RUN apt-get update && \
    apt-get -y --no-install-recommends install wget unzip && \
    apt-get clean

RUN mkdir -p ${PLUGINSDIR} && \
    cd ${PLUGINSDIR} && \
    wget https://codeload.github.com/drkloc/grafana-leaflet-choropleth-panel/zip/master && \
    unzip master && \
    mv grafana-leaflet-choropleth-panel-master grafana-leaflet-choropleth-panel && \
    sed -i "s|;plugins.*|plugins = ${PLUGINSDIR}|g" /etc/grafana/grafana.ini